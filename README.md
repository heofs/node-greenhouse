<div align="center">
<img width="250" height="250" src="https://storage.googleapis.com/ofstad-io-bucket/greenhouse.png" alt="Greenhouse image">
	<h1>node-greenhouse</h1>
	<p>
		<a href="https://github.com/heofs/node-greenhouse">Backend for greenhouse management application.</a>
	</p>
</div>

## Using

- NodeJS 10
- GraphQl
- PostgreSQL
- Firebase authentication

## Running the project locally

After filling out the secrets files, run

```bash
npm run develop
```

### Secrets files

Put firebase.json and .env file in root of the project and fill inn the fields.

firebase.json

```json
{
  "type": "",
  "project_id": "",
  "private_key_id": "",
  "private_key": "",
  "client_email": "",
  "client_id": "",
  "auth_uri": "",
  "token_uri": "",
  "auth_provider_x509_cert_url": "",
  "client_x509_cert_url": ""
}
```

.env

```bash
PG_USER=""
PG_HOST=""
PG_DATABASE=""
PG_PASSWORD=""
PG_PORT=5432
SERVER_PORT=4000
```

## Running Postgres Docker

Build and run container

`docker build -t postgres . && docker run postgres`

Check container ip address

`docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' container_name_or_id`

Check connection

`psql -p 5432 -h 172.17.0.2 -U docker`

Build and run using docker-compose

`docker-compose up --build --renew-anon-volumes`
