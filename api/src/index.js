const { ApolloServer, AuthenticationError } = require('apollo-server');
const { typeDefs } = require('./schema/typeDefs');
const { resolvers } = require('./schema/resolvers');
const { verifyToken } = require('./utils/auth');

const server = new ApolloServer({
  cors: {
    origin: '*',
    credentials: true,
  },
  typeDefs,
  resolvers,
  context: async ({ req }) => {
    const token = req.headers.authorization || '';

    // For testing
    if (token === 'testTokenCeyJhbGciOiJSU') {
      console.log('Received test token.');
      return {
        name: 'Mike Smith',
        iss: 'https://securetoken.google.com/plant-logger-auth',
        aud: 'plant-logger-auth',
        auth_time: 1557913963,
        user_id: 'fiBP0DbmSvbckqBKqwaTqXn2Unh5',
        sub: 'fiBP0DbmSvbckqBKqwaTqXn2Unh5',
        iat: 1557942973,
        exp: 1557949573,
        email: 'mike@smith.com',
        email_verified: true,
        uid: 'fiBP0DbmSvbckqBKqwaTqXn2Unh5',
      };
    }

    try {
      const user = await verifyToken(token);
      console.log(user);
      return user;
    } catch (e) {
      console.log('Access denied!');
      if (e.errorInfo.code === 'auth/id-token-expired') {
        throw new AuthenticationError('Token has expired');
      } else {
        throw new AuthenticationError('Could not verify credentials');
      }
    }
  },
});

const serverPort = process.env.SERVER_PORT || process.env.PORT || 8080;
console.log('SERVER_PORT env variable is ', serverPort);
server.listen(serverPort).then(({ url }) => {
  console.log(`Server ready at ${url}`);
});
