const db = require("../../db");
const { ApolloError } = require("apollo-server");

const getBatch = (parent, args, context) => {
  const text = "SELECT * FROM batches WHERE user_id = $1 AND id = $2";
  const values = [context.uid, args.id];
  return db
    .query(text, values)
    .then(res => {
      return res.rows[0];
    })
    .catch(e => {
      throw new ApolloError(e.message);
    });
};
const allBatches = (parent, args, context) => {
  const text = "SELECT * FROM batches WHERE user_id = $1";
  const values = [context.uid];
  return db
    .query(text, values)
    .then(res => {
      return res.rows;
    })
    .catch(e => {
      throw new ApolloError(e.message);
    });
};
const createBatch = (parent, args, context) => {
  const text =
    "INSERT INTO batches(user_id, nick, notes) VALUES($1, $2, $3) RETURNING *";
  const { nick, notes } = args.input;
  const values = [context.uid, nick, notes];

  return db
    .query(text, values)
    .then(res => {
      return res.rows[0];
    })
    .catch(e => {
      throw new ApolloError(e.message);
    });
};

const updateBatch = (parent, args, context) => {
  const { id, nick, notes } = args.input;
  const values = [context.uid, id, nick, notes];
  const text = `UPDATE batches SET
      nick = COALESCE($3, nick),
      notes = COALESCE($4, notes)
      WHERE user_id = $1 AND id = $2 RETURNING *`;

  return db
    .query(text, values)
    .then(res => {
      return res.rows[0];
    })
    .catch(e => {
      console.error(e.message);
      throw new Error(e.stack);
    });
};

const deleteBatch = (parent, args, context) => {
  const { id } = args.input;
  const values = [context.uid, id];
  const text = `DELETE FROM batches WHERE user_id = $1 AND id = $2 RETURNING *`;

  return db
    .query(text, values)
    .then(res => {
      console.log(res.rows[0]);
      if (!res.rows[0]) {
        throw new Error("Record was not found.");
      }
      return res.rows[0];
    })
    .catch(e => {
      console.error(e.message);
      throw new Error(e.stack);
    });
};

module.exports = {
  getBatch,
  allBatches,
  createBatch,
  updateBatch,
  deleteBatch
};
