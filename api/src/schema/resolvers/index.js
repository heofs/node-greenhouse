const {
  getVariety,
  allVarieties,
  createVariety,
  updateVariety,
  deleteVariety,
} = require('./varieties');
const { allPlants } = require('./plants');
const {
  getBatch,
  allBatches,
  createBatch,
  updateBatch,
  deleteBatch,
} = require('./batches');

exports.resolvers = {
  Query: {
    variety: (parent, _, context) => getVariety(parent, _, context),
    batch: (parent, _, context) => getBatch(parent, _, context),
    allVarieties: (parent, _, context) => allVarieties(parent, _, context),
    allPlants: (parent, _, context) => allPlants(parent, _, context),
    allBatches: (parent, _, context) => allBatches(parent, _, context),
  },
  Mutation: {
    createVariety: (parent, _, context) => createVariety(parent, _, context),
    updateVariety: (parent, _, context) => updateVariety(parent, _, context),
    deleteVariety: (parent, _, context) => deleteVariety(parent, _, context),
    createBatch: (parent, _, context) => createBatch(parent, _, context),
    updateBatch: (parent, _, context) => updateBatch(parent, _, context),
    deleteBatch: (parent, _, context) => deleteBatch(parent, _, context),
  },
};
