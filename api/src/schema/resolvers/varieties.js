const db = require('../../db');
const { ApolloError } = require('apollo-server');

const getVariety = (parent, args, context) => {
  const text = 'SELECT * FROM varieties WHERE user_id = $1 AND id = $2';
  const values = [context.uid, args.id];
  return db
    .query(text, values)
    .then(res => {
      return res.rows[0];
    })
    .catch(e => {
      throw new ApolloError(e.message);
    });
};

const allVarieties = (parent, args, context) => {
  const text = 'SELECT * FROM varieties WHERE user_id = $1';
  const values = [context.uid];
  return db
    .query(text, values)
    .then(res => {
      console.warn(res.rows);
      return res.rows;
    })
    .catch(e => {
      throw new ApolloError(e.message);
    });
};

const createVariety = (parent, args, context) => {
  const text =
    'INSERT INTO varieties(user_id, variety, grow_time, flower_time, notes) VALUES($1, $2, $3, $4, $5) RETURNING *';
  const { variety, grow_time, flower_time, notes } = args.input;
  const values = [context.uid, variety, grow_time, flower_time, notes];

  return db
    .query(text, values)
    .then(res => {
      return res.rows[0];
    })
    .catch(e => {
      throw new ApolloError(e.message);
    });
};

const updateVariety = (parent, args, context) => {
  const { id, variety, grow_time, flower_time, notes } = args.input;
  const values = [context.uid, id, variety, grow_time, flower_time, notes];
  const text = `UPDATE varieties SET
    variety = COALESCE($3, variety),
    grow_time = COALESCE($4, grow_time),
    flower_time = COALESCE($5, flower_time),
    notes = COALESCE($6, notes)
    WHERE user_id = $1 AND id = $2 RETURNING *`;

  return db
    .query(text, values)
    .then(res => {
      return res.rows[0];
    })
    .catch(e => {
      console.error(e.message);
      throw new Error(e.stack);
    });
};

const deleteVariety = (parent, args, context) => {
  const { id } = args.input;
  const values = [context.uid, id];
  const text = `DELETE FROM varieties WHERE user_id = $1 AND id = $2 RETURNING *`;

  return db
    .query(text, values)
    .then(res => {
      console.log(res.rows[0]);
      if (!res.rows[0]) {
        throw new Error('Record was not found.');
      }
      return res.rows[0];
    })
    .catch(e => {
      console.error(e.message);
      throw new Error(e.stack);
    });
};

module.exports = {
  getVariety,
  allVarieties,
  createVariety,
  updateVariety,
  deleteVariety,
};
