const db = require('../../db');
const { ApolloError } = require('apollo-server');

const allPlants = (parent, args, context) => {
  const text = 'SELECT * FROM plants WHERE user_id = $1';
  const values = [context.uid];
  return db
    .query(text, values)
    .then(res => {
      return res.rows;
    })
    .catch(e => {
      throw new ApolloError(e.message);
    });
};

module.exports = {
  allPlants,
};
