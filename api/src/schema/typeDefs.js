const { gql } = require('apollo-server');

const typeDefs = gql`
  type Settings {
    userId: String!
    locale: String
  }

  type Query {
    variety(id: ID): Variety!
    plant: Plant!
    batch: Batch!
    allVarieties: [Variety!]!
    allPlants: [Plant!]!
    allBatches: [Batch!]!
  }
  type Mutation {
    createVariety(input: CreateVarietyInput!): Variety
    updateVariety(input: UpdateVarietyInput!): Variety
    deleteVariety(input: DeleteVarietyInput!): Variety
    createPlant(input: CreatePlantInput!): Plant
    # updatePlant(input: UpdatePlantInput!): Plant
    # deletePlant(input: DeletePlantInput!): Plant
    createBatch(input: CreateBatchInput!): Batch
    updateBatch(input: UpdateBatchInput!): Batch
    deleteBatch(input: DeleteBatchInput!): Batch
  }
  type Variety {
    id: ID!
    variety: String!
    grow_time: Int
    flower_time: Int
    created: String
    notes: String
  }
  type Plant {
    id: ID!
    variety: Variety
    startDate: String
    created: String
  }
  type Batch {
    id: ID!
    plants: [Plant!]!
    nick: String
    created: String
    notes: String
  }
  input CreateVarietyInput {
    variety: String!
    grow_time: Int
    flower_time: Int
    notes: String
  }
  input UpdateVarietyInput {
    id: ID!
    variety: String
    grow_time: Int
    flower_time: Int
    notes: String
  }
  input DeleteVarietyInput {
    id: ID!
  }
  input CreatePlantInput {
    variety: ID
    startDate: String
  }
  input CreateBatchInput {
    nick: String!
    notes: String
  }
  input UpdateBatchInput {
    id: ID!
    nick: String
    notes: String
  }
  input DeleteBatchInput {
    id: ID!
  }
`;

module.exports = { typeDefs };
