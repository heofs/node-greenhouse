const admin = require("firebase-admin");

const serviceAccount = require("../../firebase.json");

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://plant-logger-auth.firebaseio.com"
});

const verifyToken = token => {
  return admin.auth().verifyIdToken(token);
};

module.exports = { verifyToken };
