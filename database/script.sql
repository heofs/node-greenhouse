CREATE ROLE nodeuser WITH PASSWORD '87afjn133q4bmaxzcq99' LOGIN INHERIT;

CREATE TABLE users(
    user_id CHAR(40) NOT NULL PRIMARY KEY UNIQUE,
    name CHAR(40),
    email CHAR(255),
    locale CHAR(10)
);

CREATE TABLE varieties(
    id SERIAL UNIQUE,
    user_id char(28) NOT NULL,
    variety varchar(120) NOT NULL,
    grow_time int,
    flower_time int,
    created timestamp WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
    notes text,
    primary key (user_id, variety)
);
    -- UNIQUE (user_id, variety)

CREATE TABLE batches(
    id SERIAL UNIQUE,
    user_id char(28) NOT NULL,
    name varchar(60) NOT NULL,
    created timestamp WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
    notes text,
    active BOOLEAN,
    primary key (user_id, name)
);

CREATE TABLE batch_logs(
    id SERIAL PRIMARY KEY,
    user_id char(28) NOT NULL,
    batch_id int REFERENCES batches (id) ON DELETE CASCADE NOT NULL,
    temperature real,
    humidity real,
    ppm real,
    created timestamp WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
    notes text
);

CREATE TABLE plants(
    id SERIAL PRIMARY KEY,
    user_id char(28) NOT NULL,
    active BOOLEAN,
    variety_id int REFERENCES varieties (id),
    batch_id int REFERENCES batches (id) ON DELETE CASCADE,
    start_date timestamp WITH TIME ZONE,
    end_date timestamp WITH TIME ZONE,
    created timestamp WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
    notes text
);

CREATE TABLE plant_logs(
    id SERIAL PRIMARY KEY,
    user_id char(28) NOT NULL,
    plant_id int REFERENCES plants (id) ON DELETE CASCADE,
    condition char(10),
    created timestamp WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
    notes text
);

-- GRANT ALL PRIVILEGES ON TABLE users TO "dev-user";

GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public to nodeuser;
GRANT ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA public to nodeuser;
GRANT ALL PRIVILEGES ON ALL FUNCTIONS IN SCHEMA public to nodeuser;